# Gentoo Install Documents

# BIOS Updates
- When a BIOS update is applied, dual-booting with GRUB will be broken; to fix:
    1. Boot into LiveUSB
    2. Reinstall sys-boot/GRUB
    3. Run "grub-install --target=x86_64-efi --efi-directory=/boot
    4. Make install (from /usr/src/linux)
    5. dracut --force --kver=X.X.X-gentoo
    6. grub-mkconfig -o /boot/grub/grub.cfg
    7. reboot

## Description
